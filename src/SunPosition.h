// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#ifndef SUNPOSITION_H
#define SUNPOSITION_H

class SunPosition
{

public:
    struct Position
    {
        float azimuth;
        float elevation;
    };

    SunPosition(float lon, float lat);

    // All times have to be in UTC
    Position position(int year, int month, int day, int hour, int minute, int second) const;
    float azimuth(int year, int month, int day, int hour, int minute, int second) const;
    float elevation(int year, int month, int day, int hour, int minute, int second) const;

private: // Functions
    struct SunParameters
    {
        float declination;
        float hourAngle;
    };

    int floorDiv(int a, int b) const;
    long julianDayFromDate(int year, int month, int day) const;
    SunParameters sunParameters(int year, int month, int day,
                                int hour, int minute, int second) const;
    float azimuth(const SunParameters &parameters) const;
    float elevation(const SunParameters &parameters) const;

private: // Variables
    const float m_lon;
    const float m_lat;

};

#endif // SUNPOSITION_H
