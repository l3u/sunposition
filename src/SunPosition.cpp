// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

// Local includes
#include "SunPosition.h"

#ifndef Arduino_h
// Add some defintiions provided by Arduino.h if we're not on Arduino
#include <math.h>
#define PI M_PI
#define TWO_PI M_PI * 2.0
#define DEG_TO_RAD M_PI / 180.0
#define RAD_TO_DEG 180.0 / M_PI
#define radians(deg) ((deg) * DEG_TO_RAD)
#define degrees(rad) ((rad) * RAD_TO_DEG)
#endif

SunPosition::SunPosition(float lon, float lat)
    : m_lon(radians(lon)),
      m_lat(radians(lat))
{
}

// The following Julian day calculation is adapted from Qt 5's QDateTime class

int SunPosition::floorDiv(int a, int b) const
{
    return (a - (a < 0 ? b - 1 : 0)) / b;
}

long SunPosition::julianDayFromDate(int year, int month, int day) const
{
    if (year < 0) {
        year++;
    }
    int  a = floorDiv(14 - month, 12);
    long y = (long) year + 4800 - a;
    int  m = month + 12 * a - 3;
    return day + floorDiv(153 * m + 2, 5) + 365 * y + floorDiv(y, 4) - floorDiv(y, 100)
           + floorDiv(y, 400) - 32045;
}

// The following sun position calculations are adapted from "Arduino Uno and Solar Position
// Calculations" by Dr. David Brooks (cf. https://instesre.org/ArduinoDocuments.htm ),
// with the kind permission to publish this code as an LGPL library.
// Thanks a lot for showing us how to do this with float-only precision :-)

SunPosition::SunParameters SunPosition::sunParameters(int year, int month, int day,
                                                      int hour, int minute, int second) const
{
    // Julian day and time fractions
    const long julianDay = julianDayFromDate(year, month, day);
    const long julianDay2000 = julianDay - 2451545;
    const float timeFraction = (hour + minute / 60.0 + second / 3600.0) / 24.0 - 0.5;

    // Number of Julian centuries (36525 days) since 2000-01-01 12:00:00 UTC
    const float julianCenturies = (julianDay2000 + timeFraction) / 36525.0;

    // Solar longitude
    const float solarLongitude = radians(fmod(280.46645 + 36000.76983 * julianCenturies, 360));

    // Solar mean anomaly
    const float solarMeanAnomaly = radians(fmod(357.5291 + 35999.0503 * julianCenturies, 360));

    // Sun's equation of center
    const float sunCenter = radians(
        (1.9146 - 0.004847 * julianCenturies) * sin(solarMeanAnomaly)
        + (0.019993 - 0.000101 * julianCenturies) * sin(2 * solarMeanAnomaly)
        + 0.00029 * sin(3 * solarMeanAnomaly));

    // Obliquity of the equator
    const float obliquity = radians(23 + 26 / 60.0 + 21.448 / 3600.0 - 46.815 / 3600
                                    * julianCenturies);

    // Greenwich hour angle
    const float greenwichHourAngle = radians(fmod(280.46061837 + (360 * julianDay2000) % 360
                                                  + 0.98564736629 * julianDay2000
                                                  + 360.98564736629 * timeFraction,
                                                  360.0));

    // Solar true longitude
    const float solarTrueLongitude = fmod(sunCenter + solarLongitude, TWO_PI);

    // Right ascension
    const float rightAscension = atan2(sin(solarTrueLongitude) * cos(obliquity),
                                       cos(solarTrueLongitude));

    // Declination
    const float declination = asin(sin(obliquity) * sin(solarTrueLongitude));

    // Hour angle
    const float hourAngle = greenwichHourAngle + m_lon - rightAscension;

    return { declination, hourAngle };
}

float SunPosition::azimuth(const SunParameters &parameters) const
{
    return degrees(PI + atan2(sin(parameters.hourAngle), cos(parameters.hourAngle) * sin(m_lat)
                   - tan(parameters.declination) * cos(m_lat)));
}

float SunPosition::elevation(const SunParameters &parameters) const
{
    return degrees(asin(sin(m_lat) * sin(parameters.declination)
                        + cos(m_lat) * (cos(parameters.declination) * cos(parameters.hourAngle))));
}

SunPosition::Position SunPosition::position(int year, int month, int day,
                                            int hour, int minute, int second) const
{
    const auto parameters = sunParameters(year, month, day, hour, minute, second);
    return { azimuth(parameters), elevation(parameters) };
}

float SunPosition::azimuth(int year, int month, int day, int hour, int minute, int second) const
{
    const auto parameters = sunParameters(year, month, day, hour, minute, second);
    return azimuth(parameters);
}

float SunPosition::elevation(int year, int month, int day, int hour, int minute, int second) const
{
    const auto parameters = sunParameters(year, month, day, hour, minute, second);
    return elevation(parameters);
}
