// SPDX-FileCopyrightText: none
//
// SPDX-License-Identifier: CC0-1.0

#include <SunPosition.h>

void setup()
{
    Serial.begin(115200);

    // Initialize a SunPosition object with the coordinates of Hof's old town
    const SunPosition sunPosition(11.91544, 50.31749);

    const int year = 2022;
    const int month = 6;
    const int day = 4;

    int hour = 0;
    int minute = 0;

    // Calculate the sun's azimuth and elevation for the whole day, using a 15 minute interval.
    // All times are in UTC, so the local time for that day is 2 hours later.

    while (hour < 24) {
        char buffer[8];
        sprintf(buffer, "%02d:%02d: ", hour, minute);
        Serial.print(buffer);

        const auto position = sunPosition.position(year, month, day, hour, minute, 0);
        Serial.print("azimuth: ");
        Serial.print(position.azimuth);
        Serial.print(" elevation: ");
        Serial.println(position.elevation);

        minute += 15;
        if (minute > 45) {
            minute = 0;
            hour++;
        }
    }
}

void loop()
{
}
