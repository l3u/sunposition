# SunPosition

## Sun position calculations for Arduino

This library calculates the sun's azimuth and elevation for a given location, date, and time.
The code is adapted from the publication "Arduino Uno and Solar Position Calculations" by Dr.
David Brooks (cf. <https://instesre.org/ArduinoDocuments.htm>).

The problem is that the Arduino Uno doesn't provide `double` precision, but only calculates with `float` numbers. For most solar calculations, `double` precision is needed. Dr. Brooks solved this problem in a clever way for the azimuth and elevation calculation; the present code works with `float` only precision, still yielding very accurate results.

The code isn't limited to the Arduino platform (it will compile and work on any e.g. Linux system as well), but it's intended to be used on an Arduino, esp. an Arduino without `double` math.

The said publication has been released under the [CC-BY-NC-ND]([https://creativecommons.org/licenses/by-nc-nd/4.0/) license. I was not sure how code derived from it could be used, licensed or published, so I simply asked. I got an answer from the original author with the kind permission to release this as a LGPL library, as long as a reference to the original publication was added.

Thanks for the official permit, and thanks for doing the hard work :-)

### Why not SolarPosition?

A similar library, [SolarPosition](https://github.com/KenWillmott/SolarPosition), is also based on Dr. Brooks' publication. Two factors made me not use it though: On the one side, it seemed a bit too complicated to me. I wanted to grasp the math behind this a bit better, so I wanted to keep it simple. And on the other side, this library relies on the Arduino [Time](http://playground.arduino.cc/Code/Time/) library, which seems to be unmaintained (or at least outdated).

I thus decided to implement this from scratch, without any dependencies. This makes it necessary to call the calculation functions with year, month, day, hour, minute and second, which is a bit clumsy; but a wrapper function doing the actual call and taking e.g. an [RTClib](https://github.com/adafruit/RTClib) `DateTime` object to represent the respective moment in time can be implemented trivially.

### How to use it

Put this code into your `libraries` directory. The Arduino IDE should then find it. The library does (as said) not have any dependency. Having done that, you can add it to your sketch by including it like so:

    #include <SunPosition.h>

A `SunPosition` object is initialized for a specific place. E.g. you would do something like this, possibly for a global variable (outside of the `setup` and `loop` loops):

    const SunPosition sunPosition(11.91544, 50.31749);

There are three functions you can call, each taking a year, month, day, hour, minute and second (in UTC):

  * `position`
  * `azimuth`
  * `elevation`

The first one, `position` will return a `Position` struct containing both the azimuth and elevation of the sun at the respective moment in time, for the initialized location. The other two will only return the respective value as a `float`.

All three share two parameters: the declination and the hour angle. Using the `position` function, these are only calculated once. If you need both values, use `position`; if you're only interested in one of the two parameters, use the respective function. This way, you can cheap out processor cycles.

That's it :-) Have a lot of fun with `SunPosition`!

## Ports

There's also a Python port of the library, with the exact same functionality, except that `datetime.datetime` objects are used to represent the requested moment in time the azimuth and/or elevation is calulated for instead of passing all components separately.

You can find it in the `Python` directory.
