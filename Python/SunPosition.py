# SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import math
from datetime import datetime

class SunPosition:
    def __init__(self, lon: float, lat: float):
        self.lon = math.radians(lon)
        self.lat = math.radians(lat)

    # The following Julian day calculation is adapted from Qt 5's QDateTime class

    def floorDiv(self, a: int, b: int) -> int:
        return (a - (b - 1 if a < 0 else 0)) // b;

    def julianDayFromDate(self, year: int, month: int, day: int):
        if (year < 0):
            year += 1
        a = self.floorDiv(14 - month, 12);
        y = year + 4800 - a;
        m = month + 12 * a - 3;
        return day + self.floorDiv(153 * m + 2, 5) + 365 * y + self.floorDiv(y, 4) \
               - self.floorDiv(y, 100) + self.floorDiv(y, 400) - 32045;

    # The following sun position calculations are adapted from "Arduino Uno and Solar Position
    # Calculations" by Dr. David Brooks (cf. https://instesre.org/ArduinoDocuments.htm ),
    # with the kind permission to publish this code as an LGPL library.
    # Thanks a lot for showing us how to do this with float-only precision :-)

    def sunParameters(self, datetime: datetime) -> (float, float):
        # Julian day and time fractions
        julianDay = self.julianDayFromDate(datetime.year, datetime.month, datetime.day)
        julianDay2000 = julianDay - 2451545
        timeFraction = (datetime.hour + datetime.minute / 60.0 + datetime.second / 3600.0) \
                       / 24.0 - 0.5

        # Number of Julian centuries (36525 days) since 2000-01-01 12:00:00 UTC
        julianCenturies = (julianDay2000 + timeFraction) / 36525.0

        # Solar longitude
        solarLongitude = math.radians(math.fmod(280.46645 + 36000.76983 * julianCenturies, 360))

        # Solar mean anomaly
        solarMeanAnomaly = math.radians(math.fmod(357.5291 + 35999.0503 * julianCenturies, 360))

        # Sun's equation of center
        sunCenter = math.radians(
            (1.9146 - 0.004847 * julianCenturies) * math.sin(solarMeanAnomaly)
            + (0.019993 - 0.000101 * julianCenturies) * math.sin(2 * solarMeanAnomaly)
            + 0.00029 * math.sin(3 * solarMeanAnomaly))

        # Obliquity of the equator
        obliquity = math.radians(23 + 26 / 60.0 + 21.448 / 3600.0 - 46.815 / 3600 * julianCenturies)

        # Greenwich hour angle
        greenwichHourAngle = math.radians(math.fmod(280.46061837 + (360 * julianDay2000) % 360
                                          + 0.98564736629 * julianDay2000
                                          + 360.98564736629 * timeFraction, 360.0))

        # Solar true longitude
        solarTrueLongitude = math.fmod(sunCenter + solarLongitude, math.pi * 2.0)

        # Right ascension
        rightAscension = math.atan2(math.sin(solarTrueLongitude) * math.cos(obliquity),
                                    math.cos(solarTrueLongitude))

        # Declination
        declination = math.asin(math.sin(obliquity) * math.sin(solarTrueLongitude))

        # Hour angle
        hourAngle = greenwichHourAngle + self.lon - rightAscension

        return declination, hourAngle

    def getAzimuth(self, declination: float, hourAngle: float) -> float:
        return math.degrees(math.pi + math.atan2(math.sin(hourAngle),
                                                 math.cos(hourAngle) * math.sin(self.lat)
                            - math.tan(declination) * math.cos(self.lat)))

    def getElevation(self, declination: float, hourAngle: float) -> float:
        return math.degrees(math.asin(math.sin(self.lat) * math.sin(declination)
                            + math.cos(self.lat) * (math.cos(declination) * math.cos(hourAngle))))

    # All datetimes have to be in UTC

    def position(self, datetime: datetime) -> (float, float):
        declination, hourAngle = self.sunParameters(datetime)
        return self.getAzimuth(declination, hourAngle), self.getElevation(declination, hourAngle)

    def azimuth(self, datetime: datetime) -> float:
        declination, hourAngle = self.sunParameters(datetime)
        return self.getAzimuth(declination, hourAngle)

    def elevation(self, datetime: datetime) -> float:
        declination, hourAngle = self.sunParameters(datetime)
        return self.getElevation(declination, hourAngle)
